import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.page.html',
  styleUrls: ['./alert.page.scss'],
})
export class AlertPage {
  titulo = 'Alert'
  constructor(public alertController: AlertController) { }

  async presentAlert1() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
          text: 'Ok',          
          handler: () => {
            console.log('Botton ok');
          }
        }
      ]
    });

    await alert.present();
  }
  async presentAlertPrompt2() {
    const alert = await this.alertController.create({
      header: 'Prompt!',
      subHeader:'ingrese el nombre',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Nombre'
          
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (inputs) => {
            this.titulo = inputs.name;
            console.log(inputs);
          }
        }
      ]
    });

    await alert.present();
  }

}
