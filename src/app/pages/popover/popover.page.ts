import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { PopoinfoComponent } from '../../components/popoinfo/popoinfo.component';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {

  constructor(
    private _PopoverCtrl: PopoverController
  ) { }

  ngOnInit() {
  }
  async mostratPopover(ev: any) {
    const popover = await this._PopoverCtrl.create({
      component: PopoinfoComponent,
      event: ev,
      mode:'ios',
      translucent: true,
      backdropDismiss:false
    });
     await popover.present();
    // const {data} = await popover.onDidDismiss();
    const {data} = await popover.onWillDismiss();
    console.log(data);
    
  }
  contact(){

  }
}
