import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { ComponentesI } from 'src/app/model/intefaces';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  componentes: Observable<ComponentesI[]>
  constructor(
    private _menuCtrl: MenuController,
    private _DataSrv: DataService
  ) { }

  ngOnInit() {
    this.componentes = this._DataSrv.getMenuOpts();
  }
  toogleMenu(){
    this._menuCtrl.toggle();
  }
}
