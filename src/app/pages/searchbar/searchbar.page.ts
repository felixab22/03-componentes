import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.page.html',
  styleUrls: ['./searchbar.page.scss'],
})
export class SearchbarPage implements OnInit {
  albunes:any
  textoBuscar='';
  constructor(
    private _DataSrv: DataService
  ) { }

  ngOnInit() {
    this._DataSrv.getAlbunes().subscribe(res=> {
      console.log(res);  
      this.albunes = res;    
    });
  }
  onSearchChange(event){
    console.log(event);
    this.textoBuscar = event.detail.value;
  }
}
