import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-infinite',
  templateUrl: './infinite.page.html',
  styleUrls: ['./infinite.page.scss'],
})
export class InfinitePage implements OnInit {
  @ViewChild(IonInfiniteScroll,{static: false}) infinitecroll:IonInfiniteScroll
  data = Array(20);
  constructor() { }

  ngOnInit() {
  }
  loadData(event){
    console.log('cargando...');
      setTimeout(()=>{
        const newArray = Array(20);
        this.data.push(...newArray);
        event.target.complete();
        if(this.data.length>50){
          event.target.complete();
          this.infinitecroll.disabled = true;
          return
        }
      },1000)
  }

}
