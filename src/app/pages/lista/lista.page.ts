import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';
import { IonList, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {
  @ViewChild('lista',{static: false} ) lista: IonList
  datas: Observable<any>;
  constructor(
    private _DataSrv: DataService,
    private _ToasSrv: ToastController
  ) { }

  ngOnInit() {
    this.listaGetUsers();
  }

  async presentToast(message: string) {
    const toast = await this._ToasSrv.create({
      message,
      duration: 2000,
      color:'danger'
    });
    toast.present();
  }
  listaGetUsers() {
    this.datas = this._DataSrv.getUsers();
  }
  favorite(event) {
    
    this.presentToast('Guardó en favoritos');
    this.lista.closeSlidingItems();
    
  }
  share(event) {
    this.presentToast('Compartido');
    this.lista.closeSlidingItems();
    
  }
  borrar(event) {
    this.presentToast('Borrado');
    this.lista.closeSlidingItems();
    
  }
}
