import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalInfoPage } from '../modal-info/modal-info.page';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {

  constructor(
    private _ModalCtrl: ModalController
  ) { }

  ngOnInit() {
  }
  async abrilModal(){
    const modal = await  this._ModalCtrl.create({
      component: ModalInfoPage,
      componentProps: {
        nombre: 'Felix',
        pais: 'Perú',
        phone: 967661450
      }
    });
       await modal.present();
       const { data } = await modal.onWillDismiss();
      console.log('data',data);
       
  }
  
}
