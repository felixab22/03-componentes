import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { SuperHeroesI } from 'src/app/model/intefaces';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-segment',
  templateUrl: './segment.page.html',
  styleUrls: ['./segment.page.scss'],
})
export class SegmentPage implements OnInit {
  @ViewChild('segmento',{static: true}) segment: IonSegment;
  listaHeeroes: SuperHeroesI[] = null;
  compania='';
  
  constructor(
    private _DataSvr: DataService
  ) { }

  ngOnInit() {
    this.segment.value = 'todos';
    this._DataSvr.getSuperHeroes().subscribe(res=> {
      this.listaHeeroes = res;
    });
  }
  segmentChanged(event){
    if(event.detail.value === 'todos') {
      this.compania ='';
    } else {
      this.compania = event.detail.value;
    }
  }
}
