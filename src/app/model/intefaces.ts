export interface ComponentesI {
    icon: string;
    name: string;
    redirecTo: string;
  }
  export interface SuperHeroesI {
    superhero: string;
    publisher: string;
    alter_ego: string;
    first_appearance: string;
    characters: string;
  }