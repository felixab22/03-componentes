import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';
import { ComponentesI } from '../../model/intefaces';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  componentes: Observable<ComponentesI[]>
  constructor(
    private _DataSrv: DataService
  ) { }

  ngOnInit() {

    this.componentes = this._DataSrv.getMenuOpts();
  }
}
