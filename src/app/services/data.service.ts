import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ComponentesI, SuperHeroesI } from '../model/intefaces';
import {delay} from 'rxjs/operators'
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor( 
    private _http: HttpClient
  ) { }

  getUsers(){
    return this._http.get('https://jsonplaceholder.typicode.com/users');
  }
  getMenuOpts(){
    return this._http.get<ComponentesI[]>('/assets/data/menu.json');
  }
  getAlbunes(){
    return this._http.get('https://jsonplaceholder.typicode.com/albums');
  }
  getSuperHeroes(){
    return this._http.get<SuperHeroesI[]>('/assets/data/superheroes.json').
    pipe(
      delay(2000)
    );
  }
}
